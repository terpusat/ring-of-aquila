setup:
	( \
		source env/bin/activate; \
		pip install --upgrade pip; \
		pip install -r requirements.txt; \
	)

run-dev:
	( \
		source env/bin/activate; \
		python main.py; \
	)
