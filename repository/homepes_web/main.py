import os
from fabric.api import *
from fabric.contrib import files
from helpers.send_email import *


def setEnv(config):
    env.skip_bad_hosts = True
    env.host_string = config['host']


def deploy(config, repository, branch, action, author, commit):
    setEnv(config)
    repoPath = config['repository']
    basePath = config['path']
    pathName = config['name']

    if branch == config['branch']:
        if files.exists(basePath + '/' + pathName) == False:
            with cd(basePath):
                run('git clone ' + repoPath + ' ' + pathName)
        else:
            with cd(basePath + '/' + pathName):
                run('git checkout .')
                run('git pull origin ' + config['branch'])

    # send mail to list email
    mail_config = {}
    mail_config["from"] = config['email']['from']
    mail_config["to"] = config['email']['to']
    mail_config["subject"] = config['email']['subject']
    mail_config["message"] = email_html_template(commit)

    if mail_config["message"] != '':
        email_send(mail_config, 'html')
