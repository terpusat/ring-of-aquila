####
# aquila deployment script, this script just for update and restart aquila process.
# HOW TO START AQUILA ?
# aquila start with clone repo first, then install the requirements environment
# env includes in requirements.txt base on python 2.7+
# when environment istalled, running aquila with `supervisord -c [process/YOUR_SERVER_CONF]`
# then running supervisorctl with `supervisord -c [process/YOUR_SERVER_CONF]`
# finally this deployment script can run in your machine.
####
import os
from fabric.api import *
from helpers.send_email import *


def deploy(config, repository, branch, action, author, commit):
    repoPath = config['repository']
    basePath = config['path']
    pathName = config['name']

    if branch == config['branch']:
        local('git checkout .')
        local('git pull origin ' + config['branch'])

        # send mail to list email
        mail_config = {}
        mail_config["from"] = config['email']['from']
        mail_config["to"] = config['email']['to']
        mail_config["subject"] = config['email']['subject']
        mail_config["message"] = "New engine not support email body yet"

        if mail_config["message"] != '':
            email_send(mail_config, 'html')

        # then restart
        local('supervisorctl -c process/terpusat.conf restart all')
