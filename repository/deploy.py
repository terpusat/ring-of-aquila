# dummy project
from repository.dumbass.main import deploy as dumbass

# internal tools packages
from repository.aquila.main import deploy as fountain_aquila

# system and assets packages
from repository.system_php_ci.main import deploy as system_php_ci
from repository.public_directory_php_ci.main import deploy as public_directory_php_ci

# trippedia packages
from repository.trippedia.main import deploy as trippedia

# mpw packages
from repository.mpw.main import deploy as mpw

# omkumis packages
from repository.media_omkumis.main import deploy as media_omkumis
from repository.admin_omkumis.main import deploy as admin_omkumis
from repository.front_omkumis.main import deploy as front_omkumis

# sureWhite projects
from repository.surewhite_design.main import deploy as surewhite_design

# homepes projects
from repository.homepes_api.main import deploy as homepes_api
from repository.homepes_web.main import deploy as homepes_web

'''
'''


def platform_dumbass(config, repository, branch, action, author, commit):
    dumbass(config, repository, branch, action, author, commit)


'''
MPW poenya
'''


def mpw_lintong_php(config, repository, branch, action, author, commit):
    mpw(config, repository, branch, action, author, commit)


'''
Trippedia poenya
'''


def tpd_tiwus_php(config, repository, branch, action, author, commit):
    trippedia(config, repository, branch, action, author, commit)


'''
System Sharing Code Igniter 3.x
'''


def system_code_igniter(config, repository, branch, action, author, commit):
    system_php_ci(config, repository, branch, action, author, commit)


'''
Public Directory Code Igniter 3.x
'''


def paublic_dir_code_igniter(config, repository, branch, action, author, commit):
    public_directory_php_ci(config, repository, branch, action, author, commit)


'''
Media Om Kumis media.omkumis.com
'''


def media_api_omkumis_php(config, repository, branch, action, author, commit):
    media_omkumis(config, repository, branch, action, author, commit)


'''
Admin Om Kumis administrator.omkumis.com
'''


def admin_omkumis_php(config, repository, branch, action, author, commit):
    admin_omkumis(config, repository, branch, action, author, commit)


'''
Frontend Om Kumis www.omkumis.com
'''


def frontend_omkumis_php(config, repository, branch, action, author, commit):
    front_omkumis(config, repository, branch, action, author, commit)


'''
Design for sureWhite
'''


def surewhite_design_html(config, repository, branch, action, author, commit):
    surewhite_design(config, repository, branch, action, author, commit)


'''
HomePes PHP API
'''


def homepes_api_php(config, repository, branch, action, author, commit):
    homepes_api(config, repository, branch, action, author, commit)


'''
HomePes Angular Web
'''


def homepes_web_js(config, repository, branch, action, author, commit):
    homepes_web(config, repository, branch, action, author, commit)
