__all__ = ['deploy', 'repoValidation']

import json
from repository.deploy import *
from config import repository


class repoValidation:
    engine = 'bitbucket'
    request = {}
    repository = ''
    branch = ''
    action = ''
    author = ''
    commit = ''

    def __init__(self, hookData):
        self.request = hookData
        self.engine = self.set_engine(hookData)
        self.repository = self.set_repository(hookData)
        self.branch = self.set_branch(hookData)
        self.action = self.set_action(hookData)
        self.author = self.set_author(hookData)
        self.commit = self.set_commit(hookData)

    def set_engine(self, hookData):
        try:
            return hookData['repository']['engine']
        except KeyError, NameError:
            return "bitbucket"

    def set_repository(self, hookData):
        try:
            return hookData['repository']['name']
        except KeyError, NameError:
            return False

    def set_branch(self, hookData):
        try:
            if 'push' in hookData:
                return hookData['push']['changes'][0]['new']['name']
            
            return 'master'
        except KeyError, NameError:
            return 'master'

    def set_action(self, hookData):
        try:
            if 'push' in hookData:
                return 'push'
        except KeyError, NameError:
            return False

    def set_author(self, hookData):
        try:
            return hookData['actor']['display_name'] + ' (' + hookData['actor']['username'] + ')'
        except KeyError, NameError:
            return False

    def set_commit(self, hookData):
        try:
            if 'push' in hookData:
                return hookData['push']['changes'][0]['new']['target']['hash']
        except KeyError, NameError:
            return False

    def is_valid_hook(self):
        if (self.engine != "bitbucket" or (self.repository != False and
            self.branch != False and
            self.action != False and
            self.author != False and
                self.commit != False)):
            self.execute_fabric_repo()
            
            return True
        return False

    def execute_fabric_repo(self):
        registeredRepository = repository.registered.get(self.repository)
        if registeredRepository is not None:
            execute_func = getattr(deploy, registeredRepository['func'])
            execute_func(registeredRepository, self.repository,
                         self.branch, self.action, self.author, self.request)

    def success(self, customMessage=''):
        if customMessage != '':
            return json.dumps({
                'message': customMessage,
                'status': 8
            })
        return json.dumps({
            'message': 'Success Deploy',
            'status': 8
        })

    def error(self, customMessage=''):
        if customMessage != '':
            return json.dumps({
                'message': customMessage,
                'status': 0
            })
        return json.dumps({
            'message': 'Error Deploy',
            'status': 0
        })
