# Fabfile to:
#    - update the remote system(s)
#    - download and install an application

# Import Fabric's API module
import os
from fabric.api import *
from helpers.send_email import *

env.hosts = [
    # 'localhost'
    # 'server.domain.tld',
    # 'ip.add.rr.ess
    # 'server2.domain.tld',
]
# Set the username
env.user = "root"

# Set the password [NOT RECOMMENDED]
# env.password = "mautauajah"


def deploy(config, repository, branch, action, author, commit):
    # path variables
    repoPath = config['repository']
    basePath = config['path']
    pathName = config['name']
    originNginxConfigPath = config['env']['nginx']['from']
    sysNginxConfigPath = config['env']['nginx']['to']

    if branch == config['branch']:
        if os.path.isdir(basePath) == False:
            with lcd('/data/apps'):
                local('git clone ' + repoPath + ' ' + pathName)
        else:
            with lcd(basePath):
                local('git checkout .')
                local('git pull origin ' + config['branch'])
        with lcd(basePath):
            local('cp ' + originNginxConfigPath + ' ' + sysNginxConfigPath)
            commandTest = local('nginx -t')
            if commandTest.return_code == 0:
                local('nginx -s reload')

    # send mail to list email
    mail_config = {}
    mail_config["from"] = config['email']['from']
    mail_config["to"] = config['email']['to']
    mail_config["subject"] = config['email']['subject']
    mail_config["message"] = email_html_template(commit)

    if mail_config["message"] != '':
        email_send(mail_config, 'html')
