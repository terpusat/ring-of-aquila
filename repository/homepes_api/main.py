import os
import time
import datetime
from fabric.api import *
from fabric.contrib import files
from helpers.send_email import *
from helpers.exec_command import git


def setEnv(config):
    env.skip_bad_hosts = True
    env.host_string = config['host']

def common():
    run('php artisan config:clear')
    run('composer install --optimize-autoloader --no-dev')
    run('php artisan key:generate')
    run('chmod -R 0777 storage')
    run('rm ~/git.key')

def deploy(config, repository, branch, action, author, commit):
    setEnv(config)
    repoPath = config['repository']
    basePath = config['path']
    pathName = config['name']

    if branch == config['branch']:
        if files.exists(basePath + '/' + pathName) == False:
            with cd(basePath):
                run('git clone ' + repoPath + ' ' + pathName)
                common()
        else:
            with cd(basePath + '/' + pathName):
                ts = time.time()
                st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

                local('scp ~/.ssh/id_rsa root@' + config['host'] + ':~/git.key')
                run('test -f ~/git.key || chmod 0400 ~/git.key')
                run(git('git stash save ' + st))
                run(git('git pull origin ' + config['branch']))
                common()

        # send mail to list email
        mail_config = {}
        mail_config["from"] = config['email']['from']
        mail_config["to"] = config['email']['to']
        mail_config["subject"] = config['email']['subject']
        mail_config["message"] = "Gitlab engine not support email body yet"

        if mail_config["message"] != '':
            email_send(mail_config, 'html')
