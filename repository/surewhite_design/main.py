import os
from fabric.api import *
from helpers.send_email import *

env.hosts = ['terpusat.com']


def deploy(config, repository, branch, action, author, commit):
    repoPath = config['repository']
    basePath = config['path']
    pathName = config['name']

    if branch == config['branch']:
        if os.path.isdir(basePath + '/' + pathName) == False:
            with lcd(basePath):
                local('git clone ' + repoPath + ' ' + pathName)
        else:
            with lcd(basePath + '/' + pathName):
                local('git checkout .')
                local('git pull origin ' + config['branch'])

    # send mail to list email
    mail_config = {}
    mail_config["from"] = config['email']['from']
    mail_config["to"] = config['email']['to']
    mail_config["subject"] = config['email']['subject']
    mail_config["message"] = email_html_template(commit)

    if mail_config["message"] != '':
        email_send(mail_config, 'html')
