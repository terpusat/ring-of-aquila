registered = {
    'Ring of Aquila': {
        'func': 'fountain_aquila',
        'repository': 'git@bitbucket.org:terpusat/ring-of-aquila.git',
        'path': '/data/apps/fountain',
        'name': 'aquila',
        'branch': 'master',
        'email': {
            'from': 'prakasa@devetek.com',
            'to': 'prakasa@devetek.com,arivin29@devetek.com,kusumaindraputra@gmail.com',
            'subject': 'Fountain - Aquila'
        },
    },
    'Tiwus - PHP Version': {
        'func': 'tpd_tiwus_php',
        'repository': 'git@bitbucket.org:permatanusantaragroup/tiwus-php-version.git',
        'path': '/data/apps/trippedia.co.id',
        'name': 'trippedia.co.id',
        'branch': 'master',
        'email': {
            'from': 'prakasa@devetek.com',
            'to': 'prakasa@devetek.com,office@mypermatawisata.com,adeheryshopyan@gmail.com',
            'subject': 'MPW Hooks'
        },
        'hosts': [],
    },
    'Lintong - PHP Version': {
        'func': 'tpd_tiwus_php',
        'repository': 'git@bitbucket.org:permatanusantaragroup/lintong-php-version.git',
        'path': '/data/apps/mypermatawisata.com',
        'name': 'mypermatawisata.com',
        'branch': 'master',
        'email': {
            'from': 'prakasa@devetek.com',
            'to': 'prakasa@devetek.com,office@mypermatawisata.com,adeheryshopyan@gmail.com',
            'subject': 'My Permata Wisata - New Push To Repository'
        },
        'hosts': [],
    },
    'Apps Media': {
        'func': 'media_api_omkumis_php',
        'repository': 'git@bitbucket.org:terpusat/apps-media.git',
        'path': '/data/apps/media.omkumis.com',
        'name': 'media.omkumis.com',
        'branch': 'master',
        'email': {
            'from': 'prakasa@devetek.com',
            'to': 'prakasa@devetek.com,admin@omkumis.com',
            'subject': 'Om Kumis Hooks'
        },
        'hosts': [],
    },
    'Apps Backend': {
        'func': 'admin_omkumis_php',
        'repository': 'git@bitbucket.org:terpusat/apps-backend.git',
        'path': '/data/apps/administrator.omkumis.com',
        'name': 'administrator.omkumis.com',
        'branch': 'master',
        'email': {
            'from': 'prakasa@devetek.com',
            'to': 'prakasa@devetek.com,admin@omkumis.com',
            'subject': 'Om Kumis Hooks'
        },
        'hosts': [],
        'env': {
            'nginx': {
                'from': 'env/nginx/omkumis',
                'to': '/etc/nginx/sites-available/default',
            },
            'supervisor': '',
        },
    },
    'System': {
        'func': 'system_code_igniter',
        'repository': 'git@bitbucket.org:terpusat/system.git',
        'path': '/data/system',
        'name': 'system',
        'branch': 'master',
        'email': {
            'from': 'prakasa@devetek.com',
            'to': 'prakasa@devetek.com,admin@omkumis.com',
            'subject': 'CI System Hooks'
        },
    },
    'Public Html': {
        'func': 'paublic_dir_code_igniter',
        'repository': 'git@bitbucket.org:terpusat/public-html.git',
        'path': '/data/public_html',
        'name': 'public_html',
        'branch': 'master',
        'email': {
            'from': 'prakasa@devetek.com',
            'to': 'prakasa@devetek.com,admin@omkumis.com',
            'subject': 'Public Directory Hooks'
        },
    },
    'Frontend Om Kumis': {
        'func': 'frontend_omkumis_php',
        'repository': 'git@bitbucket.org:terpusat/frontend-om-kumis.git',
        'path': '/data/apps/www.omkumis.com',
        'name': 'www.omkumis.com',
        'branch': 'master',
        'email': {
            'from': 'prakasa@devetek.com',
            'to': 'prakasa@devetek.com,admin@omkumis.com',
            'subject': 'Om Kumis Hooks'
        },
        'hosts': [],
    },
    'Lanaya': {
        'func': 'surewhite_design_html',
        'repository': 'git@bitbucket.org:terpusat/lanaya.git',
        'path': '/data/public_html_dev',
        'name': 'sureWhite',
        'branch': 'master',
        'email': {
            'from': 'prakasa@devetek.com',
            'to': 'prakasa@devetek.com,me@danirus.com,rizki2fauzi@gmail.com,arivin29@devetek.com',
            'subject': 'sureWhite Hooks'
        },
    },
    'KM api admin': {
        'func': 'homepes_api_php',
        'repository': 'git@gitlab.com:devetek/cheese.git',
        'path': '/srv/hompes/apidev.hompes.id',
        'name': 'site',
        'branch': 'master',
        'email': {
            'from': 'prakasa@devetek.com',
            'to': 'prakasa@devetek.com,arivin29@devetek.com,kusumaindraputra@gmail.com,adhyaksaj@gmail.com',
            'subject': 'homepesAPI Hooks'
        },
        'host': '157.230.39.111',
    },
    'versi_web': {
        'func': 'homepes_web_js',
        'repository': 'git@bitbucket.org:devetek/versi_web.git',
        'path': '/srv/hompes/dev.hompes.id',
        'name': 'public_html',
        'branch': 'master',
        'email': {
            'from': 'prakasa@devetek.com',
            'to': 'prakasa@devetek.com,arivin29@devetek.com,kusumaindraputra@gmail.com,adhyaksaj@gmail.com',
            'subject': 'homepesWEB Platform'
        },
        'host': '157.230.39.111',
    },
    'Dumbass': {
        'func': 'platform_dumbass',
        'repository': 'git@bitbucket.org:devetek/dumbass.git',
        'path': '/app',
        'name': 'dumbass',
        'branch': 'master',
        'email': {
            'from': 'prakasa@devetek.com',
            'to': 'prakasa@devetek.com',
            'subject': 'Platform - Dumbass'
        },
        'host': '157.230.39.111'
    },
}
