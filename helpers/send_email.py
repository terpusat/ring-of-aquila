import datetime
from email.mime.text import MIMEText
from subprocess import Popen, PIPE

SENDMAIL = '/usr/sbin/sendmail'

"""[Helper to exec sendmail binary]
params:
  @ config <dict>
    Available key:
      - from
      - to
      - subject
      - message
"""


def email_send(config, content_type='plain'):
    msg = MIMEText(config['message'], content_type)
    msg["From"] = config['from']
    msg["To"] = config['to']
    msg["Subject"] = config['subject']
    p = Popen([SENDMAIL, '-t', '-oi'], stdin=PIPE)
    p.communicate(msg.as_string())


def email_html_template(data={}):
    date_now = datetime.datetime.now()
    year = date_now.year

    if 'push' not in data:
        return ''

    # user
    username = data['actor']['username']
    fullname = data['actor']['display_name']
    avatar = data['actor']['links']['avatar']['href']

    # repository
    repo_name = data['repository']['name']
    repo_link = data['repository']['links']['html']['href']
    repo_avatar = data['repository']['links']['avatar']['href']

    # action
    action_time = data['push']['changes'][0]['commits'][0]['date']
    action_hash = data['push']['changes'][0]['commits'][0]['hash']
    action_link = data['push']['changes'][0]['commits'][0]['links']['html']['href']
    action_description = data['push']['changes'][0]['commits'][0]['summary']['html']

    css = "img{border:0;-ms-interpolation-mode:bicubic;max-width:100%}body{background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}table{border-collapse:separate;mso-table-lspace:0;mso-table-rspace:0;width:100%}table td{font-family:sans-serif;font-size:14px;vertical-align:top}.body{background-color:#f6f6f6;width:100%}.container{display:block;margin:0 auto!important;max-width:580px;padding:10px;width:580px}.content{box-sizing:border-box;display:block;margin:0 auto;max-width:580px;padding:10px}.main{background:#fff;border-radius:3px;width:100%}.wrapper{box-sizing:border-box;padding:20px}.content-block{padding-bottom:10px;padding-top:10px}.footer{clear:both;margin-top:10px;text-align:center;width:100%}.footer td,.footer p,.footer span,.footer a{color:#999;font-size:12px;text-align:center}h1,h2,h3,h4{color:#000;font-family:sans-serif;font-weight:400;line-height:1.4;margin:0;margin-bottom:30px}h1{font-size:35px;font-weight:300;text-align:center;text-transform:capitalize}p,ul,ol{font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;margin-bottom:15px}p li,ul li,ol li{list-style-position:inside;margin-left:5px}a{color:#3498db;text-decoration:underline}.btn{box-sizing:border-box;width:100%}.btn>tbody>tr>td{padding-bottom:15px}.btn table{width:auto}.btn table td{background-color:#fff;border-radius:5px;text-align:center}.btn a{background-color:#fff;border:solid 1px #3498db;border-radius:5px;box-sizing:border-box;color:#3498db;cursor:pointer;display:inline-block;font-size:14px;font-weight:bold;margin:0;padding:12px 25px;text-decoration:none;text-transform:capitalize}.btn-primary table td{background-color:#3498db}.btn-primary a{background-color:#3498db;border-color:#3498db;color:#fff}.last{margin-bottom:0}.first{margin-top:0}.align-center{text-align:center}.align-right{text-align:right}.align-left{text-align:left}.clear{clear:both}.mt0{margin-top:0}.mb0{margin-bottom:0}.preheader{color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0}.powered-by a{text-decoration:none}hr{border:0;border-bottom:1px solid #f6f6f6;margin:20px 0}@media only screen and (max-width:620px){table[class=body] h1{font-size:28px!important;margin-bottom:10px!important}table[class=body] p,table[class=body] ul,table[class=body] ol,table[class=body] td,table[class=body] span,table[class=body] a{font-size:16px!important}table[class=body] .wrapper,table[class=body] .article{padding:10px!important}table[class=body] .content{padding:0!important}table[class=body] .container{padding:0!important;width:100%!important}table[class=body] .main{border-left-width:0!important;border-radius:0!important;border-right-width:0!important}table[class=body] .btn table{width:100%!important}table[class=body] .btn a{width:100%!important}table[class=body] .img-responsive{height:auto!important;max-width:100%!important;width:auto!important}}@media all{.ExternalClass{width:100%}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%}.apple-link a{color:inherit!important;font-family:inherit!important;font-size:inherit!important;font-weight:inherit!important;line-height:inherit!important;text-decoration:none!important}.btn-primary table td:hover{background-color:#34495e!important}.btn-primary a:hover{background-color:#34495e!important;border-color:#34495e!important}}"
    html = '''
    <!doctype html>
      <html>
        <head>
          <meta name="viewport" content="width=device-width" />
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
          <title>Platform - Devetek</title>
          <style>{CSS}</style>
        </head>
        <body class="">
          <span class="preheader">This is preheader text. Some clients will show this text as a preview.</span>
          <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
            <tr>
              <td>&nbsp;</td>
              <td class="container">
                <div class="content">

                  <!-- START CENTERED WHITE CONTAINER -->
                  <table role="presentation" class="main">

                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                      <td class="wrapper">
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td>
                              <p>{AUTHOR} melakukan update ke repository <a href="{REPO_LINK}">{REPO_NAME}</a> pada {TIME}</p>
                              <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                                <tbody>
                                  <tr>
                                    <td align="left">
                                      <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                          <tr>
                                            <td> <a href="{ACTION_LINK}" target="_blank">Lihat Selengkapnya</a> </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <p><b>Commit Hash: </b></p>
                              <p>{ACTION_HASH}</p>
                              <p><b>Commit Description: </b></p>
                              {ACTION_DESCRIPTION}
                              <p><b>Author: </b></p>
                              <p>{USERNAME}</p>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>

                  <!-- END MAIN CONTENT AREA -->
                  </table>
                  <!-- END CENTERED WHITE CONTAINER -->

                  <!-- START FOOTER -->
                  <div class="footer">
                    <table style="text-align:center" role="presentation" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td class="content-block powered-by">
                          &copy; {NOW}, Devetek Inc.
                        </td>
                      </tr>
                    </table>
                  </div>
                  <!-- END FOOTER -->

                </div>
              </td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </body>
      </html>
    '''

    formated_message = html.format(CSS=css, NOW=year, TIME=action_time, USERNAME=username, AUTHOR=fullname,
                                   REPO_NAME=repo_name, REPO_LINK=repo_link, ACTION_HASH=action_hash, ACTION_LINK=action_link, ACTION_DESCRIPTION=action_description)

    return formated_message
